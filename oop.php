<?php
    class ClassParent
    {
        public function __call($name, $arguments)
        {
            if($name === 'hitung_persegi'){
                return $arguments[0]*$arguments[0];
            }elseif($name === 'hitung_persegi_panjang'){
                return $arguments[0]*$arguments[1];
            }elseif($name === 'hitung_segitiga'){
                return $arguments[0]*$arguments[1]*1/2;
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="" method="post">
        <select name="pilihan" id="">
            <option value="">Pilih</option>
            <option value="persegi">Hitung Persegi</option>
            <option value="persegi_panjang">Hitung Persegi Panjang</option>
            <option value="segitiga">Hitung Segitiga</option>
        </select>
        <input type="text" name="var1" id="">
        <input type="text" name="var2" id="">
        <input type="submit" value="Submit">
    </form>
</body>
</html>
<?php
    if($_POST){
        $hitung = new ClassParent();
        if($_POST['pilihan']=='persegi'){
            echo $hitung->luas_persegi($_POST['var1']);
        }elseif($_POST['pilihan']=='persegi_panjang'){
            echo $hitung->luas_persegi_panjang($_POST['var1'], $_POST['var2']);
        }elseif($_POST['pilihan']=='segitiga'){
            echo $hitung->luas_segitiga($_POST['var1'], $_POST['var2']);
        }
    }
?>